/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.pier.irc

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class IrcPierTest {
    @Test
    fun testAntiPing() {
        assertEquals("k\u200Bit\u200Bte\u200Bn", rebuildWithAntiPing("kitten"))
        assertEquals("k\u200Bit\u200Bte\u200Bn \u200B\uD83C\uDF57\u200B", rebuildWithAntiPing("kitten \uD83C\uDF57"))
    }
}
