/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.util

import java.util.jar.Manifest

private const val JAR_PATH_TO_VERSIONING_INFO = "dis4irc-versioning.txt"

/**
 * Fetches and provides versioning information from the manifest specified in [JAR_PATH_TO_VERSIONING_INFO]
 */
object Versioning {
    /**
     * Gets the build version of this jar
     */
    val version: String

    /**
     * Gets the top of tree git hash this version was built against
     */
    val gitHash: String

    /**
     * Gets the source repo of this project
     */
    val sourceRepo: String

    init {
        val resources = this.javaClass.classLoader.getResources(JAR_PATH_TO_VERSIONING_INFO)
        var verOut = "Unknown version"
        var gitHashOut = "Unknown Git Commit"
        var repoOut = "Unknown source repo"

        if (resources.hasMoreElements()) {
            resources.nextElement().openStream().use {
                with(Manifest(it).mainAttributes) {
                    if (getValue("Name") != "Pardol") {
                        return@use
                    }

                    verOut = getValue("Version")
                    gitHashOut = getValue("Git-Hash")
                    repoOut = getValue("Source-Repo")
                }
            }
        }

        version = verOut
        gitHash = gitHashOut
        sourceRepo = repoOut
    }
}
