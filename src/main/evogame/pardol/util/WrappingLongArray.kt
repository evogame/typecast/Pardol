/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.util

/**
 * Light wrapper around LongArrays to remove old entries as new ones are added
 */
class WrappingLongArray(private val size: Int) {
    private val backing = LongArray(size)
    private var index = 0
    private var hasWrappedAround = false

    /**
     * Adds an element to the underlying array
     *
     * If this array is fully populated, the oldest element is replaced with the newly added element
     */
    fun add(e: Long) {
        wrapIndex()
        backing[index] = e
        index += 1
    }

    /**
     * Returns a copy of the backing LongArray up to the latest element that has been populated
     *
     * This does NOT return a full size array unless it has been fully populated
     */
    fun toLongArray(): LongArray {
        return if (hasWrappedAround) {
            backing.copyOf()
        } else {
            backing.copyOf(index)
        }
    }

    private fun wrapIndex() {
        if (index == size) {
            index = 0
            hasWrappedAround = true
        }
    }
}