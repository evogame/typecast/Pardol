/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.util

import evogame.pardol.bridge.message.Message
import java.io.File
import kotlin.random.Random

/** Counts the number of occurrences of the substring in the base string */
fun countSubstring(baseStr: String, subStr: String): Int = baseStr.split(subStr).size - 1

/**
 * Given a string, find the target and replace it, optionally requiring whitespace separation to
 * replace
 */
fun replaceTarget(
        base: String,
        target: String,
        replacement: String,
        requireSeparation: Boolean = true
): String {
    var out = base

    fun isWhiteSpace(i: Int): Boolean {
        return i == -1 || i == out.length || !requireSeparation || out[i].isWhitespace()
    }

    var start = out.indexOf(target, 0)
    while (start > -1) {
        val end = start + target.length
        val nextSearchStart = start + replacement.length

        if (isWhiteSpace(start - 1) && isWhiteSpace(end)) {
            out = out.replaceFirst(target, replacement)
        }

        start = out.indexOf(target, nextSearchStart)
    }

    return out
}

fun rollDie(sides: Int, count: Int = 1): Int {
    var num = 
        if (count == 0)
            1
        else
            count
    var tot = 0
    while (num > 0) {
        tot += (1..sides).random()
        num--
    }
    return tot
}

fun File.chooseLine(): String {
    var res: String = ""
    this.useLines {
        for ((n, line) in it.withIndex()) if (Random.nextDouble() <= 1.0 / (n + 1))
                res = TextReplace(line)
    }
    return res
}

fun File.chooseLine(line: String, command: Message? = null, replaceText: Boolean = true): String {
    var res: String = "Nope. That doesn't exist."
    this.forEachLine {
        val ln = it.split("=", ignoreCase = true, limit = 2)
        if (ln[0] == line) {
            res = TextReplace (ln[1], command, replaceText)
        }
    }
    return res
}

fun TextReplace(text: String, command: Message? = null, replaceText: Boolean = true): String {
    var res: String
    if (text.contains("][")) {
        val opt = text.split ("][", ignoreCase = true)
        val cnt = rollDie(opt.size) - 1
        res = opt[cnt]
    } else {
        res = text
    }
    val roll = """%roll:(\d{0,3})d(\d{1,3})%""".toRegex()
    val file = """%file:([a-zA-Z0-9]+):([a-zA-Z0-9]+)""".toRegex()
    
    res = res.replace("\\n", "\n")
    if (command != null) {
        res = res.replace("%sender%", command.sender.displayName)
    }
    res =
        file.replace(res) { m ->
            File(m.groups[1].toString() + "/" + m.groups[2].toString()).readText()
        }
    res = roll.replace(res) { m -> rollDie(m.groups[2]!!.value.toInt()).toString() }
    
    if (replaceText) {
        return res
    } else {
        return text
    }
}

fun Authorised(name: String): Boolean {
    var allowed: Boolean = false
    File("Data/Help/Authorised.txt").forEachLine { if (name.lowercase() == it) allowed = true }

    return allowed
}

