/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.config

import evogame.pardol.logger
import org.spongepowered.configurate.ConfigurationOptions
import org.spongepowered.configurate.CommentedConfigurationNode
import org.spongepowered.configurate.hocon.HoconConfigurationLoader
import org.spongepowered.configurate.loader.HeaderMode
import java.io.IOException
import java.nio.file.Path
import java.nio.file.Paths

private const val HEADER = "Dis4IRC Configuration File"

/**
 * Responsible for interacting with the underlying configuration system
 */
class Configuration(pathIn: String) {

    /**
     * The config file for use
     */
    private val configPath: Path = Paths.get(pathIn)

    /**
     * Our configuration loader
     */
    private val configurationLoader: HoconConfigurationLoader = HoconConfigurationLoader.builder()
        .path(configPath)
        .defaultOptions(ConfigurationOptions.defaults().header(HEADER))
        .headerMode(HeaderMode.PRESET)
        .build()

    /**
     * Root configuration node
     */
    private var rootNode: CommentedConfigurationNode

    /**
     * Reads configuration file and prepares for use
     */
    init {
        try {
            rootNode = configurationLoader.load()
        } catch (ex: IOException) {
            logger.error("Could not load configuration! $ex")
            throw ex
        }
    }

    /**
     * Writes config back to file
     */
    public fun saveConfig(): Boolean {
        try {
            configurationLoader.save(rootNode)
        } catch (ex: IOException) {
            logger.error("Could not save configuration file!")
            ex.printStackTrace()
            return false
        }

        return true
    }

    /**
     * Gets a child node from the root node
     */
    public fun getNode(vararg keys: String): CommentedConfigurationNode {
        return rootNode.node(*keys)
    }
}
