/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.pier.irc

import evogame.pardol.bridge.message.PlatformType
import evogame.pardol.bridge.message.Sender
import evogame.pardol.bridge.message.Source
import org.kitteh.irc.client.library.element.Channel
import org.kitteh.irc.client.library.element.User
import java.util.Optional

fun Channel.asBridgeSource(): Source = Source(this.name, null, PlatformType.IRC)
fun User.asBridgeSender(): Sender = Sender(this.nick, null, this.account.toNullable())
fun <T : Any> Optional<T>.toNullable(): T? = this.orElse(null)
