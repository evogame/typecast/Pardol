/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.pier.discord

import evogame.pardol.bridge.message.BOT_SENDER
import evogame.pardol.bridge.message.Message
import evogame.pardol.bridge.message.PlatformType
import evogame.pardol.bridge.message.sourceFromUnknown
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class DiscordJoinQuitListener(private val pier: DiscordPier) : ListenerAdapter() {
    private val logger = pier.logger

    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {
        val channel = event.guild.systemChannel
        val source = channel?.asBridgeSource() ?: sourceFromUnknown(PlatformType.DISCORD)

        // don't bridge itself
        if (pier.isThisBot(source, event.user.idLong)) {
            return
        }

        val receiveTimestamp = System.nanoTime()
        logger.debug("DISCORD JOIN ${event.user.name}")

        val sender = BOT_SENDER
        val message = Message("${event.user.name} has joined the Discord", sender, source, receiveTimestamp)
        pier.sendToBridge(message)
    }

    override fun onGuildMemberRemove(event: GuildMemberRemoveEvent) {
        val channel = event.guild.systemChannel
        val source = channel?.asBridgeSource() ?: sourceFromUnknown(PlatformType.DISCORD)

        // don't bridge itself
        if (pier.isThisBot(source, event.user.idLong)) {
            return
        }

        val receiveTimestamp = System.nanoTime()
        logger.debug("DISCORD PART ${event.user.name}")

        val sender = BOT_SENDER
        val message = Message("${event.user.name} has left the Discord", sender, source, receiveTimestamp)
        pier.sendToBridge(message)
    }
}
