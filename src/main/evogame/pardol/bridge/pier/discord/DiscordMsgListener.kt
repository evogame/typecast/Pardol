/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.pier.discord

import evogame.pardol.bridge.message.Message
import evogame.pardol.bridge.message.Sender
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

/**
 * Responsible for listening to incoming discord messages and filtering garbage
 */
class DiscordMsgListener(private val pier: DiscordPier) : ListenerAdapter() {
    private val logger = pier.logger

    override fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        // dont bridge itself
        val source = event.channel.asBridgeSource()
        if (pier.isThisBot(source, event.author.idLong)) {
            return
        }

        // don't bridge empty messages (discord does this on join)
        if (event.message.contentDisplay.isEmpty() && event.message.attachments.isEmpty()) {
            return
        }

        val receiveTimestamp = System.nanoTime()
        logger.info("DISCORD MSG ${event.channel.name} ${event.author.name}: ${event.message.contentStripped}")

        val message = event.message.toBridgeMsg(logger, receiveTimestamp)
        pier.sendToBridge(message)
    }
}
