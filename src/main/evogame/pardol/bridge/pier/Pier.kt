/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.pier

import evogame.pardol.bridge.message.Message

interface Pier {

    /**
     * Starts a pier, connecting it to whatever backend it needs, and readying it for use
     */
    fun start()

    /**
     * Called when the bridge is to be safely shutdown
     */
    fun onShutdown()

    /**
     * Sends a message through this pier
     */
    fun sendMessage(targetChan: String, msg: Message)
}
