/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.message

data class Source(
    /**
     * Name of the channel
     */
    val channelName: String,
    /**
     * Discord ID of the channel
     */
    val discordId: Long?,
    /**
     * Source type
     */
    val type: PlatformType
)

fun sourceFromUnknown(platform: PlatformType): Source { // TODO - better solutions elsewhere?
    return Source("Unknown", 0, platform)
}
