/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.message

internal val BOT_SENDER = Sender("Bridge", null, null)

data class Sender(
    /**
     * User's display name, this is *not* guaranteed to be unique or secure
     */
    val displayName: String,
    /**
     * User's discord snowflake id, or null if the message originated from Discord
     */
    val discordId: Long?,
    /**
     * User's nickserv account name, or null if the message originated from IRC or the IRC network doesn't support it
     */
    val ircNickServ: String?
)
