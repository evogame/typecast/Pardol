/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.message

enum class PlatformType {
    /**
     * Discord platform
     */
    DISCORD,
    /**
     * IRC platform
     */
    IRC
}
