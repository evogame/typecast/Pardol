/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.mutator.mutators

import evogame.pardol.bridge.message.Message
import evogame.pardol.bridge.message.PlatformType
import evogame.pardol.bridge.mutator.api.Mutator
import evogame.pardol.bridge.pier.irc.ANTI_PING_CHAR

/**
 * Strips anti-ping characters.
 */
class StripAntiPingCharacters : Mutator {

    override fun mutate(message: Message): Mutator.LifeCycle {
        if (message.source.type == PlatformType.IRC) {
            message.contents = message.contents.replace(ANTI_PING_CHAR.toString(), "")
        }
        return Mutator.LifeCycle.CONTINUE
    }
}
