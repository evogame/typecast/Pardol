/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.mutator.mutators

import evogame.pardol.bridge.message.Message
import evogame.pardol.bridge.message.PlatformType
import evogame.pardol.bridge.mutator.api.Mutator

/**
 * Blocks @here and @everyone from being sent from IRC to Discord
 */
class BlockHereEveryone : Mutator {

    override fun mutate(message: Message): Mutator.LifeCycle {
        // only block from IRC -> Discord, allow them the other way around
        if (message.source.type != PlatformType.IRC) {
            return Mutator.LifeCycle.CONTINUE
        }

        var out = message.contents

        out = out.replace("@everyone", "at-everyone")
        out = out.replace("@here", "at-here")

        message.contents = out

        return Mutator.LifeCycle.CONTINUE
    }
}
