/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.mutator.api

import evogame.pardol.bridge.message.Message

/**
 * A mutator takes the given message contents and alters it in some way before returning it
 */
interface Mutator {
    /**
     * Called on a given message to mutate the contents
     *
     * @return how to proceed with the message's lifecycle
     */
    fun mutate(message: Message): LifeCycle

    /**
     * Mutator Life Cycle control types
     */
    enum class LifeCycle {
        /**
         * Continue the lifecycle by passing this message onto the next
         */
        CONTINUE,
        /**
         * Stop the lifecycle and discard the message entirely
         */
        STOP_AND_DISCARD,
        /**
         * Stop the lifecycle and return the message as it exists currently
         */
        RETURN_EARLY
    }
}
