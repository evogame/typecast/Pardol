/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.command.executors.*
import evogame.pardol.bridge.message.BOT_SENDER
import evogame.pardol.bridge.message.Destination
import evogame.pardol.bridge.message.Message
import java.io.File
import org.spongepowered.configurate.CommentedConfigurationNode

const val COMMAND_PREFIX: String = "!"

/** Responsible for managing, looking up, and delegating to command executors */
class CommandManager(private val bridge: Bridge, config: CommentedConfigurationNode) {
    private val executorsByCommand = HashMap<String, Executor>()
    private val logger = bridge.logger
    private val bName = bridge.config.bridgeName

    init {
        val helpNode = config.node("help", "enabled")
        if (helpNode.virtual()) {
            helpNode.set("true")
        }
        if (helpNode.boolean) {
            registerExecutor("help", HelpCommand(bridge))
            registerExecutor("basics", HelpCommand(bridge))
            registerExecutor("discord", HelpCommand(bridge))
            registerExecutor("youtube", HelpCommand(bridge))
            registerExecutor("patreon", HelpCommand(bridge))
            registerExecutor("motd", HelpCommand(bridge))
            registerExecutor("writers", HelpCommand(bridge))
            registerExecutor("beta", HelpCommand(bridge))
        }

        val sysNode = config.node("system", "enabled")
        if (sysNode.virtual()) {
            sysNode.set("true")
        }
        if (sysNode.boolean) {
            registerExecutor("wakeup", SystemCommands(bridge))
            registerExecutor("restart", SystemCommands(bridge))
            registerExecutor("enhance", SystemCommands(bridge))
            registerExecutor("upgrade", SystemCommands(bridge))
        }

        val statsNode = config.node("stats", "enabled")
        if (statsNode.virtual()) {
            statsNode.set("true")
        }
        if (statsNode.boolean) {
            registerExecutor("stats", StatsCommand(bridge))
        }

        val pinnedNode = config.node("pinned", "enabled")
        if (pinnedNode.virtual()) {
            pinnedNode.set("true")
        }
        if (pinnedNode.boolean) {
            registerExecutor("pinned", PinnedMessagesCommand(bridge))
        }

        val eggsNode = config.node("eggs", "enabled")
        if (eggsNode.virtual()) {
            eggsNode.set("true")
        }
        if (eggsNode.boolean) {
            File("Data/" + bName + "/Eggs.txt").forEachLine() {
                if (it.first().toString() != "#") {
                    val ln = it.split("=", ignoreCase = true, limit = 2)
                    registerExecutor(ln[0], EasterEggs(bridge))
                }
            }
        }

        val snarkNode = config.node("snark", "enabled")
        if (snarkNode.virtual()) {
            snarkNode.set("true")
        }
        if (snarkNode.boolean) {
            registerExecutor("snark", SnarkCommand(bridge))
        }

        val stripNode = config.node("strip", "enabled")

        if (stripNode.virtual()) {
            stripNode.set("false")
        }

        if (stripNode.boolean) {
            registerExecutor("strip", StripCommand(bridge))
            registerExecutor("daily", StripCommand(bridge))
        }

        val rollNode = config.node("roll", "enabled")
        if (rollNode.virtual()) {
            rollNode.set("true")
        }
        if (rollNode.boolean) {
            registerExecutor("roll", RollCommand(bridge))
            registerExecutor("coin", RollCommand(bridge))
        }

        val arrokNode = config.node("arrok", "enabled")
        if (arrokNode.virtual()) {
            arrokNode.set("false")
        }
        if (arrokNode.boolean) {
            registerExecutor("hex", ArrokCommands(bridge))
            registerExecutor("tribe", ArrokCommands(bridge))
        }

        val weaponNode = config.node("weapon", "enabled")
        if (weaponNode.virtual()) {
            weaponNode.set("false")
        }
        if (weaponNode.boolean) {
            registerExecutor("weapon", WeaponCommands(bridge))
        }

        val catsNode = config.node("cats", "enabled")
        if (catsNode.virtual()) {
            catsNode.set("false")
        }
        if (catsNode.boolean) {
            registerExecutor("cats", CatsCommand(bridge))
        }

        val maximNode = config.node("maxim", "enabled")
        if (maximNode.virtual()) {
            maximNode.set("true")
        }
        if (maximNode.boolean) {
            registerExecutor("maxim", MaximCommand(bridge))
        }

        val hundredNode = config.node("hundred", "enabled")
        if (hundredNode.virtual()) {
            hundredNode.set("true")
        }
        if (hundredNode.boolean) {
            val fil = File("Data/100").listFiles()

            for (pFile in fil) {
                registerExecutor(pFile.nameWithoutExtension.lowercase(), HundredCommand(bridge))
            }
        }
    }

    /** Registers an executor to the given command */
    private fun registerExecutor(name: String, executor: Executor) {
        executorsByCommand[name] = executor
    }

    /** Gets the executor for the given command */
    private fun getExecutorFor(name: String): Executor? {
        return executorsByCommand[name]
    }

    /** Process a command message, passing it off to the registered executor */
    fun processCommandMessage(command: Message) {
        val strip = command.contents.substring(COMMAND_PREFIX.length, command.contents.length)
        val split = strip.split(" ")
        val trigger = split[0].lowercase() // strip off command prefix
        command.contents = strip
        val executor = getExecutorFor(trigger) ?: return

        logger.info("Passing command to executor: $executor")
        command.destination = Destination.BOTH // we want results to go to both sides by default
        val result = executor.onCommand(command)

        if (result != null) {
            command.contents = result
            command.sender = BOT_SENDER

            // submit as new message
            bridge.submitMessage(command)
        }
    }
}
