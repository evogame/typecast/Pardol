/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.api

import evogame.pardol.bridge.message.Message

interface Executor {
    /**
     * Perform some action when a command is executed
     *
     * @return your desired output or null if you desire no output
     */
    fun onCommand(command: Message): String?
}
