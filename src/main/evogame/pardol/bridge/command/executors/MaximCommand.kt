/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File

class MaximCommand(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        var rtn = "From the 70 Maxims of Maximally Effective Mercenaries:\n>>> **Maxim "
        val comm = command.contents.split(" ", ignoreCase = true, limit = 2)
        val potFile =
                when (comm[0]) {
                    "maxim" -> File("Data/Maxims.txt")
                    else -> {
                        null
                    }
                }

        if (comm.size > 1) {

            if (comm[1].toInt() > 70 || comm[1].toInt() < 1) {
                rtn =
                        "Have they written some new maxims I've not heard about? I'll have to ask Petey about those..."
            } else {
                rtn += comm[1] + "**\n*" + potFile!!.chooseLine(comm[1]) + "*"
            }
        } else {
            val rnd = rollDie(70)
            rtn += rnd.toString() + "**\n*" + potFile!!.chooseLine(rnd.toString()) + "*"
        }

        return rtn
    }
}
