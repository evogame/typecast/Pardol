/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File
import kotlin.math.*
import kotlin.text.Regex

class RollCommand(private val bridge: Bridge) : Executor {
    val bName = bridge.config.bridgeName
    val logger = bridge.logger

    override fun onCommand(command: Message): String? {
        var rtn: String
        val comm = command.contents.lowercase()
        with(comm) {
            when {
                comm.startsWith("roll", false) -> {
                    if (comm.length > 4) {
                        val cmd = comm.substring(5, comm.length)
                        val rolls = cmd.split(" ").map { it }.toTypedArray()
                        rtn =
                                when (rolls[0]) {
                                    "startrek", "sta", "trek" -> parseRolls(Ruleset.TREK, *rolls)
                                    "dnd", "d20" -> parseRolls(Ruleset.D20, *rolls)
                                    "free", "freeform" -> parseRolls(Ruleset.FREE, *rolls)
                                    else -> {
                                        parseRolls(Ruleset.FREE, *rolls)
                                    }
                                }
                    } else {
                        rtn = "Har Har! I guess you wanted a die roll..."
                    }
                }
                comm.startsWith("coin", false) -> {
                    rtn = File("Data/" + bName + "/Roll.txt").chooseLine("coin") + "\n"
                    rtn +=
                            if (rollDie(2) == 1)
                                    File("Data/" + bName + "/Roll.txt").chooseLine("heads")
                            else File("Data/" + bName + "/Roll.txt").chooseLine("tails")
                }
                else -> {
                    rtn = "What am I doing here? This code shouldn't run!"
                }
            }
        }
        return rtn
    }

    fun parseRolls(ruleset: Ruleset, vararg rolls: String): String {
        var rtn = ""
        when (ruleset) {
            Ruleset.D20, Ruleset.FREE -> {
                var oldroll = ""
                var roll: String
                for (rs in rolls) {
                    if (rs == "adv" || rs == "dis") {
                        if (oldroll == "" && (rs == "adv" || rs == "dis")) {
                            rtn =
                                    "You need to have a valid roll before you can ask for advantage or disadvantage."
                            continue
                        } else {
                            roll = oldroll
                        }
                    } else {
                        roll = rs
                    }
                    oldroll = roll
                    if (rtn.length >= 1990) {
                        rtn = "Nope! That's too much! I'm not gonna break Discord."
                        break
                    }
                    if (roll == "dnd" || roll == "d20") {
                        if (rolls.size == 1) rtn = "Har Har! I guess you wanted a DnD die roll..."
                    } else if (roll == "free" || roll == "freeform") {
                        if (rolls.size == 1)
                                rtn = "Har Har! I guess you wanted a freeform die roll..."
                    } else {
                        val regex =
                                when (ruleset) {
                                    Ruleset.D20 ->
                                            Regex(
                                                    "^(t)?(\\d{0,3})d(4|6|8|12|100?|20)((\\+|\\-)?(\\d{0,3}))?$"
                                            )
                                    else -> {
                                        Regex(
                                                "^(t)?(\\d{0,3})d(65536|\\d{1,3})((\\+|\\-)?(\\d{0,3}))?$"
                                        )
                                    }
                                }
                        val match = regex.find(roll)
                        logger.info("Die Roller Regex matches: " + match?.groupValues.toString())
                        val res = match?.groupValues
                        if (res == null) {
                            rtn += File("Data/" + bName + ".txt").chooseLine("uhoh")
                            continue
                        } else if (res[3] == "" || (res[4] != "" && res[5] == "")) {
                            rtn += File("Data/" + bName + "/Roll.txt").chooseLine("fakedie")
                            continue
                        }
                        val die = res[3].toInt()
                        logger.info("Die Sides: " + die)
                        val num =
                                if (res[2] == "") {
                                    1
                                } else {
                                    res[2].toInt()
                                }
                        logger.info("Number of Dice: " + num)
                        if (num == 0) {
                            rtn += File("Data/" + bName + "/Roll.txt").chooseLine("zerodie")
                            continue
                        }
                        var count = num
                        if (res[1] == "t" || res[5] == "+" || res[5] == "-") {
                            logger.info("Totaliser activated")
                            rtn +=
                                    File("Data/" + bName + "/Roll.txt").chooseLine("snark") +
                                            "\nI rolled " +
                                            roll +
                                            " and you got a total of "
                            var tot = 0
                            while (count > 0) {
                                val r = rollDie(die)
                                tot += r
                                logger.info("Roll " + (num - count + 1) + "/" + num + ": " + r + " (" + tot + ")")
                                count--
                            }
                            when (res[5]) {
                                "+" -> {
                                    val add =
                                        if (res[6] == "") {
                                                0
                                            } else {
                                                res[6].toInt()
                                            }
                                    tot += add
                                    logger.info("Adding " + add + " to total: " + tot)
                                }
                                "-" -> {
                                    val sub =
                                        if (res[6] == "") {
                                                0
                                            } else {
                                                res[6].toInt()
                                            }
                                    tot -= sub
                                    logger.info("subtracting " + sub + " from total: " + tot)
                                }
                                else -> {
                                    if (res[1] == "t") {
                                        logger.info("Totalising: " + tot)
                                    } else {
                                        rtn = "What am I doing here? This code shouldn't run!"
                                    }
                                }
                            }
                            
                            rtn += tot
                        } else {
                            rtn +=
                                    File("Data/" + bName + "/Roll.txt").chooseLine("snark") +
                                            "\nI rolled " +
                                            roll +
                                            "! You got the following: "
                            while (count > 0) {
                                if (rtn.length >= 1990) {
                                    rtn =
                                            File("Data/" + bName + "/Roll.txt")
                                                    .chooseLine("toomanyrolls")
                                    break
                                }
                                val r = rollDie(die)
                                /*if (die==20) {
                                    when (r) {
                                        20 -> rtn += ":star2: "
                                        1 -> rtn += ":facepalm: "
                                        else -> {
                                            rtn += r
                                        }
                                    }
                                } else {*/
                                rtn += r
                                // }
                                if (count > 1) rtn += ", "
                                count--
                            }
                        }
                        if (rolls.size > 1) rtn += "\n"
                    }
                }
            }
            Ruleset.TREK -> {
                var rollType = TrekRollType.NONE
                for (roll in rolls) {
                    when (roll.lowercase()) {
                        "sta", "trek", "startrek" ->
                                if (rolls.size == 1)
                                        rtn = "Har Har! I guess you wanted a Star Trek die roll..."
                        "task", "t" -> rollType = TrekRollType.TASK
                        "combat", "comb", "c" -> rollType = TrekRollType.COMBAT
                        "assist", "a" -> rollType = TrekRollType.ASSIST
                        else -> {
                            // TODO: Star Trek Adventures rolls and tools
                            when (rollType) {
                                TrekRollType.TASK -> {
                                    // rtn += "I'll do task rolls some day..."
                                    var comps = HashMap<String, Int>()
                                    val components =
                                            listOf("atr", "dis", "foc", "mom", "thr", "det", "ass")
                                    for (c in components) {
                                        comps.put(c, 0)
                                    }
                                    for (comp in roll.lowercase().split(":")) {
                                        val reg = Regex("^([a-z]{3})(\\d{1,2})$")
                                        val res = reg.find(comp)!!.groupValues
                                        comps.put(res[1], res[2].toInt())
                                    }
                                    var dice = 2
                                    val addOns = listOf("mom", "thr", "det")
                                    for (a in addOns) {
                                        dice += floor(sqrt((comps[a]!! * 2) + 0.25) - 0.5).toInt()
                                    }
                                    dice.coerceIn(2, 5)
                                    rtn +=
                                            "Q's got nothing on me... I've rolled for " +
                                                    roll.lowercase() +
                                                    " and you have: "
                                    var count = dice
                                    var successes = 0
                                    var crits = 0
                                    var complications = 0
                                    while (count > 0) {
                                        val r = rollDie(20)
                                        when (r) {
                                            1 -> {
                                                crits++
                                                rtn += ":star2: "
                                            }
                                            20 -> {
                                                complications++
                                                rtn += ":facepalm: "
                                            }
                                            else -> {
                                                if (comps["foc"]!! > 0 && r <= comps["dis"]!!) {
                                                    crits++
                                                    rtn += ":star2: "
                                                } else if (r <= (comps["atr"]!! + comps["dis"]!!)) {
                                                    successes++
                                                    rtn += "✓ "
                                                } else {
                                                    rtn += "x "
                                                }
                                            }
                                        }
                                        count--
                                    }
                                    rtn +=
                                            "\n That's a total of " +
                                                    (successes + (crits * 2)) +
                                                    " successes, and " +
                                                    complications +
                                                    " complications"
                                }
                                TrekRollType.COMBAT -> {
                                    rtn += "I'll do combat rolls some day..."
                                }
                                TrekRollType.ASSIST -> {
                                    // rtn += "I'll do assist rolls some day..."
                                    var comps = HashMap<String, Int>()
                                    val components = listOf("atr", "dis", "foc")
                                    for (c in components) {
                                        comps.put(c, 0)
                                    }
                                    for (comp in roll.lowercase().split(":")) {
                                        val reg = Regex("^([a-z]{3})(\\d{1,2})$")
                                        val res = reg.find(comp)!!.groupValues
                                        comps.put(res[1], res[2].toInt())
                                    }
                                    rtn +=
                                            "Awww! How cute! You're trying to help your little friend. Very well, I've rolled for " +
                                                    roll.lowercase() +
                                                    "and you got: "
                                    val r = rollDie(20)
                                    if (comps["foc"]!! > 0 && r <= comps["dis"]!!) {
                                        rtn += "✓ "
                                    } else if (r <= (comps["atr"]!! + comps["dis"]!!)) {
                                        rtn += "✓ "
                                    } else {
                                        rtn += "x "
                                    }
                                }
                                TrekRollType.NONE -> {
                                    rtn +=
                                            "What do you want me to do? Don't worry, I can wait while your braincell figures out what you did wrong."
                                    continue
                                }
                            }

                            rollType = TrekRollType.NONE
                        }
                    }
                    if (rolls.size > 1) rtn += "\n"
                }
            }
        }
        return rtn
    }
}

enum class Ruleset {
    D20,
    FREE,
    TREK
}

enum class TrekRollType {
    TASK,
    COMBAT,
    ASSIST,
    NONE
}
