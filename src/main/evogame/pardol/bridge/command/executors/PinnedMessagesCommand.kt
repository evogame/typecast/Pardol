/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.bridge.message.PlatformType
import evogame.pardol.bridge.mutator.mutators.TranslateFormatting

class PinnedMessagesCommand(private val bridge: Bridge) : Executor {
    override fun onCommand(command: Message): String? {
        if (command.source.type != PlatformType.IRC) {
            bridge.logger.debug("Ignoring request for pinned messages because it originates from Discord")
            return null
        }

        val mappedChannel = bridge.channelMappings.getMappingFor(command.source) ?: throw IllegalStateException("No mapping for source channel: ${command.source}?!?")
        val pinnedMessages = bridge.discordConn.getPinnedMessages(mappedChannel) ?: return null

        for (msg in pinnedMessages) {
            bridge.mutatorManager.applyMutator(TranslateFormatting::class.java, msg)
            val senderInfo = bridge.ircConn.createMessagePrefix(msg)
            var msgContent = msg.contents

            if (msg.attachments != null && msg.attachments.isNotEmpty()) {
                msg.attachments.forEach { msgContent += " $it"}
            }

            bridge.ircConn.sendNotice(command.sender.displayName, "$senderInfo $msgContent")
        }

        return null // don't send a message publicly
    }

}
