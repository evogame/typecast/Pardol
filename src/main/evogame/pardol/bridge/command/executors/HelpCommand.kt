/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.chooseLine
import java.io.File

class HelpCommand(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
        val bName = bridge.config.bridgeName
        var rtn: String
        when (comm[0]) {
            "help", "basics" -> rtn = File("Data/Help/" + bName + "/Basics.txt").readText()
            "patreon", "youtube", "discord" ->
                    rtn =
                            File("Data/" + bName + ".txt").chooseLine(comm[0]) +
                                    File("Data/Help/" + bName + "/" + comm[0] + "Link.txt")
                                            .readText()
            "writers", "beta" ->
                    rtn =
                            File("Data/" + bName + ".txt").chooseLine(comm[0] + "1") +
                                    File("Data/Help/WritersLink.txt").readText() +
                                    File("Data/" + bName + ".txt").chooseLine(comm[0] + "2")
            "motd" -> {
                if (comm.size > 2 && comm[1] == "set") {
                    File("Data/Help/" + bName + "/MOTD.txt").writeText(comm[2])
                    rtn = "New Message of The Day set"
                } else if (comm.size == 2 && comm[1] == "set") {
                    rtn = File("Data/" + bName + ".txt").chooseLine(comm[0])
                } else {
                    rtn = File("Data/Help/" + bName + "/MOTD.txt").readText()
                }
            }
            else -> {
                rtn = "I shouldn't be running this code"
            }
        }

        return rtn
    }
}
