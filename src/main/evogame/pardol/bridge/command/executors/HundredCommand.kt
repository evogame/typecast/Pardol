/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File

class HundredCommand(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message) : String? {
        var rtn = ""
        val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
/*      val potFile = when (comm[0]) {
            "potion" -> File("Data/Potions.txt")
            "mushroom", "shroom" -> File("Data/Shrooms.txt")
            "stew", "soup" -> File("Data/Stew.txt")
            "haiku" -> File("Data/Haikus.txt")
            else -> {
                null
            }
        }*/
        val potFile = File("Data/100/" + comm[0].lowercase() + ".txt")
        val count: Int = try { potFile.chooseLine("#count").toInt() } catch (e: NumberFormatException) { 100 }

        if (comm.size > 1) {
            rtn += potFile.chooseLine(comm[1])
        } else {
            rtn += potFile.chooseLine(rollDie(count).toString())
        }

        return rtn
    }
}