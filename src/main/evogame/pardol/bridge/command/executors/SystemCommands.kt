/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File
import kotlinx.coroutines.*

class SystemCommands(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        val comm = command.contents.split(" ", ignoreCase = true, limit = 2)
        val bName = bridge.config.bridgeName
        val rtn =
                when (comm[0]) {
                    "wakeup", "restart" -> {
                        if (Authorised(command.sender.displayName)) {
                            Restart()
                            File("Data/" + bName + ".txt").chooseLine("restartY", command)
                        } else {
                            File("Data/" + bName + ".txt").chooseLine("restartN", command)
                        }
                    }
                    "enhance", "upgrade" -> {
                        val ver = if (comm.size > 1) comm[1] else null
                        if (Authorised(command.sender.displayName)) {
                            Update(ver)
                            File("Data/" + bName + ".txt").chooseLine("upgradeY", command)
                        } else {
                            File("Data/" + bName + ".txt").chooseLine("upgradeN", command)
                        }
                    }
                    else -> {
                        "Do you think I enjoy responding to gibberish?"
                    }
                }
        return rtn
    }

    private fun Restart() {
        bridge.logger.info("Process restart requested")
        GlobalScope.launch {
            delay(1000)
            Runtime.getRuntime().exec("systemctl restart pardol")
        }
    }

    private fun Update(version: String? = null) {
        val logger = bridge.logger
        logger.info("Current Version: " + Versioning.version)
        val ver = version ?: Versioning.version
        logger.info("Requested Version: " + ver)
        val dir = File("/opt/pardol")
        logger.info("Updating...")
        GlobalScope.launch {
            delay(1000)
            Runtime.getRuntime().exec("./update.sh -v ${ver}", null, dir)
            Runtime.getRuntime().exec("git pull", null, dir)
        }
    }
}
