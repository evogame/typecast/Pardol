/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File

class SnarkCommand(private val bridge: Bridge) : Executor {
    override fun onCommand(command: Message): String? {
        val bName = bridge.config.bridgeName
        var rtn = ""
        val snarkFile = File("Data/" + bName + "/Snark.txt")
        val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
        /*val sender = message.Sender.discordId
        val role = bridge.discordConn.*/
        if ( comm.size > 1 ) {
            when (comm[1]) {
                "add" /* && memberRole = Staff */ -> {
                    if(Authorised(command.sender.displayName)){
                        if (comm.size == 2) {
                            rtn += "What do you want me to add? I'm not a mind-reader!"
                        } else {
                            if ((1..100).random() < 100) {
                                rtn += "Ooooh, that's a good one! I'll have to add it to my list.\n"
                                snarkFile.appendText("\n" + comm[2])
                            } else {
                                rtn += "Arbitrary is my new favourite word, so...\nNOPE, that's not good enough. Try a better pun\n"
                            }
                        }
                    }else{
                        rtn += "I don't think you're funny enough for me to do that."
                    }
                }
                "config.SnarkLevel=11" -> {
                    rtn += "Are you sure? You actually want me to turn my snark level DOWN?"
                }
                else -> {
                    rtn += snarkFile.chooseLine()
                }
            }
        } else {
            rtn += snarkFile.chooseLine()
        }
        return rtn
    }
}