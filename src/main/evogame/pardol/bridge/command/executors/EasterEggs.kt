/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File

class EasterEggs(private val bridge: Bridge) : Executor {
        val bName = bridge.config.bridgeName

        override fun onCommand(command: Message): String? {
                val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
                val rt = !(comm.size > 1 && comm[1].lowercase() == "alloptions")

                val rtn = File("Data/" + bName + "/Eggs.txt").chooseLine(comm[0].lowercase(), command, rt)

                return rtn
        }
}
