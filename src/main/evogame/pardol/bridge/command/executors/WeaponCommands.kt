/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File

class WeaponCommands(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        var rtn = ""
        val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
        val manFile =
                if (comm.size > 1) {
                    File("Data/Weapon/Manufacturers.txt").chooseLine(comm[1])
                } else {
                    File("Data/Weapon/Manufacturers.txt").chooseLine()
                }
        rtn +=
                if (comm.size > 2) {
                    ">>> **" +
                            comm[1] +
                            " " +
                            comm[2] +
                            "**\n" +
                            File("Data/Weapon/" + manFile + ".txt").chooseLine(comm[2])
                } else {
                    val rnd = rollDie(20)
                    "For this job, it sounds like you'll need a \n" +
                            ">>> **" +
                            comm[1] +
                            rnd.toString() +
                            "**\n" +
                            File("Data/Weapon/" + manFile + ".txt")
                                    .chooseLine(
                                            rnd.toString()
                                    ) // TODO : Need a better way to handle this
                }

        return rtn
    }
}
