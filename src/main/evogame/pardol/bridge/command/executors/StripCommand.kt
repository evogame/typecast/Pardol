/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*

class StripCommand(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        var rtn = ""
        val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
        if (comm.size == 1 || comm[1] == "random") {
            rtn += "https://www.schlockmercenary.com/strip/" + rollDie(7351)
        } else if (comm[1] == "today") {
            /*val date =
            rtn += potFile!!.chooseLine()*/
        } else {
            rtn += "https://www.schlockmercenary.com/" + comm[1]
        }

        return rtn
    }
}
