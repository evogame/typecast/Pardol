/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import java.io.File

class CatsCommand(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        val comm = command.contents.split(" ", ignoreCase = true, limit = 2)

        val rtn =
                if (comm.size > 1) {
                    when (comm[1].lowercase()) {
                        "history" -> File("Cats/History.txt").readText()
                        "mittens", "ranger" -> File("Cats/Mittens.txt").readText()
                        "shadow", "sorcerer", "scorcherer" -> File("Cats/Shadow.txt").readText()
                        "tacocat", "cleric" -> File("Cats/Tacocat.txt").readText()
                        "lucky", -> File("Cats/Lucky.txt").readText()
                        "spike", "bard" -> File("Cats/Spike.txt").readText()
                        "lady", "lsp", "ladyscratchempaw", "rogue" ->
                                File("Cats/LadyScratchemPaw.txt").readText()
                        else -> {
                            "COMING SOON: The continuing story of the Cat Adventuring Party!"
                        }
                    }
                } else {
                    "COMING SOON: The continuing story of the Cat Adventuring Party!"
                }

        return rtn
    }
}
