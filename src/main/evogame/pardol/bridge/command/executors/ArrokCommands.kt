/*
 * This file is part of Pardol. A fork of Dis4IRC 1.2.
 *
 * Copyright (c) 2020-2023 Pardol contributors
 * Copyright (c) 2018-2020 Dis4IRC by zachbr & contributors
 *
 * MIT License
 */

package evogame.pardol.bridge.command.executors

import evogame.pardol.bridge.Bridge
import evogame.pardol.bridge.command.api.Executor
import evogame.pardol.bridge.message.Message
import evogame.pardol.util.*
import java.io.File
import kotlin.math.*

class ArrokCommands(private val bridge: Bridge) : Executor {

    override fun onCommand(command: Message): String? {
        var rtn = ""
        val comm = command.contents.split(" ", ignoreCase = true, limit = 3)
        val debug = if (comm.size == 3) comm[2].toInt() else null
        when (comm[0]) {
            "hex" -> {
                val hx = if (comm.size == 1) "explore" else comm[1]
                when (hx) {
                    "explore" -> {
                        rtn += "Terrain: \n"
                        rtn += createHexTile("terrain")
                        rtn += "Resources: \n"
                        rtn += createHexTile("resources")
                        rtn += "What there is to find: \n"
                        rtn += createHexTile("what")
                    }
                    else -> {
                        rtn += createHexTile(comm[1], debug)
                    }
                }
            }
            "tribe" -> {
                val trb = if (comm.size == 1) "new" else comm[1]
                when (trb) {
                    "new" -> {
                        rtn += createTribe("race")
                        rtn += createTribe("religion")
                        rtn += createTribe("attitude")
                    }
                    else -> {
                        rtn += createTribe(comm[1], debug)
                    }
                }
            }
        }
        return rtn
    }

    private fun createHexTile(stage: String, arg: Int? = null): String {
        var rtn = ""
        when (stage) {
            "terrain" -> {
                val r1 = if (arg != null) 20 else rollDie(20)
                rtn += r1
                val ter =
                        if (r1 > 8) {
                            val r2 = arg ?: rollDie(20)
                            rtn += ", " + r2
                            when (r2) {
                                1 -> "Desert"
                                in 2..3 -> "Grassland"
                                in 4..6 -> "Forest"
                                in 7..8 -> "Jungle"
                                in 9..10 -> "Swamp"
                                in 11..13 -> "Hills"
                                in 14..15 -> "Mountains"
                                in 16..17 -> "Cliffs"
                                in 18..19 -> "Lake"
                                20 -> "Wasteland"
                                else -> {
                                    "Previous"
                                }
                            }
                        } else {
                            "Previous"
                        }
                rtn += " - " + File("Data/Arrok/Terrain.txt").chooseLine(ter) + "\n"
            }
            "resources" -> {
                var bRes = false
                var r = IntArray(3)
                for (i in (0..2)) {
                    r[i] = rollDie(20)
                    rtn += r[i]
                    if (i < 2) rtn += ", "
                }
                for (i in (0..2)) {
                    when (r[i]) {
                        in 1..15 -> continue
                        in 16..20 -> {
                            if (!bRes) {
                                bRes = true
                                rtn += " - You have discovered the following resources:\n"
                            }
                            var r2 = arg ?: rollDie(20)
                            rtn +=
                                    r2.toString() +
                                            " - " +
                                            File("Data/Arrok/Resources.txt")
                                                    .chooseLine(r2.toString()) +
                                            "\n"
                        }
                    }
                }
                if (!bRes) rtn += " - This land contains no useful resources.\n"
            }
            "what" -> {
                val r1 = if (arg != null) 20 else rollDie(20)
                rtn += r1
                if (r1 > 15) {
                    val r2 = arg ?: rollDie(20)
                    rtn += ", " + r2
                    val haz =
                            when (r2) {
                                in 1..3 -> "Weather"
                                in 4..5 -> "Hazard"
                                6 -> "Cave"
                                7 -> "Spirits"
                                8 -> "Spring"
                                9 -> "Bones"
                                10 -> "Magic"
                                11 -> "Trails"
                                12 -> "Windfall"
                                13 -> "Trap"
                                in 14..15 -> "Battlefield"
                                in 16..17 -> "Camp"
                                18 -> "Markings"
                                19 -> "Landmark"
                                20 -> "Calamity"
                                else -> {
                                    ""
                                }
                            }
                    val r3 =
                            when (haz) {
                                "Weather",
                                "Hazard",
                                "Cave",
                                "Bones",
                                "Trap",
                                "Markings",
                                "Calamity" -> rollDie(20)
                                "Spirits", "Magic" -> rollDie(8)
                                else -> {
                                    0
                                }
                            }
                    if (r3 > 0) rtn += ", " + r3
                    if (haz == "Cave" && r3 == 20) rtn += ", " + rollDie(6)
                    if (haz == "Markings" && r3 == 20) rtn += ", " + rollDie(20)
                    rtn += " - " + File("Data/Arrok/HexWhat.txt").chooseLine(haz) + "\n"
                } else {
                    rtn += " - Nothing special going on here. Nope. Nothing to see.\n"
                }
            }
        }
        return rtn
    }

    private fun createTribe(stage: String, arg: Int? = null): String {
        var rtn = ""
        when (stage) {
            "race" -> {
                val r1 = arg ?: rollDie(20)
                rtn += r1
                val race =
                        when (r1) {
                            1 -> "Boggers"
                            2 -> "Croga"
                            3 -> "Rhynoks"
                            in 4..5 -> "Gorian"
                            6 -> "Imperadons"
                            in 7..8 -> "Janth"
                            in 9..10 -> "Meekin"
                            11 -> "Shellkin"
                            12 -> "Tanuki"
                            13 -> "Teryx"
                            in 14..15 -> "Ursan"
                            in 16..17 -> "Veldtaur"
                            in 18..19 -> "Wolfen"
                            20 -> {
                                val r2 = rollDie(20)
                                rtn += ", " + r2
                                when (r2) {
                                    in 1..4 -> "Fey"
                                    in 5..8 -> "Fiends"
                                    in 9..12 -> "Celestials"
                                    in 13..16 -> "Shadows"
                                    in 17..18 -> "Elementals"
                                    in 19..20 -> "Weird"
                                    else -> {
                                        "More code that will never run"
                                    }
                                }
                            }
                            else -> {
                                "This code will never run"
                            }
                        }
                val r3 = rollDie(20)
                rtn += ", " + r3
                val size =
                        when (r3) {
                            in 1..7 -> "small"
                            in 8..14 -> "medium"
                            in 15..20 -> "large"
                            else -> {
                                "Something broke if this code runs"
                            }
                        }
                rtn +=
                        " - You have found a " +
                                size +
                                " tribe (" +
                                r3 * 5 +
                                " members) consisting mainly of "
                rtn +=
                        if (race != "Weird")
                                race + ":\n> " + File("Data/Arrok/Tribes.txt").chooseLine(race)
                        else File("Data/Arrok/WeirdTribes.txt").chooseLine()
                rtn += "\n"
            }
            "religion" -> {
                val r1 = arg ?: rollDie(20)
                rtn += r1
                val rel =
                        when (r1) {
                            in 1..5 -> "Ancestors"
                            in 6..10 -> "Monster"
                            in 11..15 -> {
                                val r2 = rollDie(8)
                                rtn += ", " + r2
                                when (r2) {
                                    1 -> "Sun"
                                    2 -> "Pit"
                                    3 -> "Feywild"
                                    4 -> "Shadowfell"
                                    5 -> "Air"
                                    6 -> "Earth"
                                    7 -> "Fire"
                                    8 -> "Water"
                                    else -> {
                                        "More useless code"
                                    }
                                }
                            }
                            in 16..20 -> "Natural"
                            else -> {
                                "This code will never see the light of day"
                            }
                        }
                rtn +=
                        " - Careful study of the tribe's habits shows evidence of " +
                                File("Data/Arrok/Religions.txt").chooseLine(rel) +
                                ".\n"
            }
            "attitude" -> {
                val r1 = arg ?: rollDie(20)
                rtn += r1
                val att =
                        when (r1) {
                            1 -> "Arrogant"
                            2 -> "Brave"
                            3 -> "Cautious"
                            4 -> "Cunning"
                            5 -> "Curious"
                            6 -> "Desperate"
                            7 -> "Driven"
                            8 -> "Gregarious"
                            9 -> "Honor-bound"
                            10 -> "Hostile"
                            11 -> "Hot-headed"
                            12 -> "Insular"
                            13 -> "Lazy"
                            14 -> "Menacing"
                            15 -> "Peaceful"
                            16 -> "Protective"
                            17 -> "Savage"
                            18 -> "Suspicious"
                            19 -> "Stubborn"
                            20 -> "Zealous"
                            else -> {
                                "Why does Kotlin require this code?"
                            }
                        }
                rtn +=
                        " - The general attitude of the tribe's members is best described as " +
                                att +
                                ":\n> " +
                                File("Data/Arrok/Attitudes.txt").chooseLine(att) +
                                ".\n"
            }
        }

        return rtn
    }
}
