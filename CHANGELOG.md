# Changelog
User-relevant changes to the software, see the full commit log for all changes.  
[Downloads](https://github.com/zachbr/Dis4IRC/releases)

## 0.2.4
[Commits since 0.2.3](https://gitlab.com/evogame/typecast/Pardol/-/compare/v0.2.3...v0.2.4)
* Die roller (`roll` command) for `dnd` ruleset is functional with support for totals and +1's etc.
* Max die count increased to 999, though Discord will limit messages to 2000 characters, so too many rolls will be cancelled (with an approriate snarky
  message).
* Added `ethan` shortcut to auto-roll a `1d4`.

## 0.2.3
[Commits since 0.2.2](https://gitlab.com/evogame/typecast/Pardol/-/compare/ba4f8fac...v0.2.3)
* First pass at a die roller command.
* The command `roll` followed by a parameter in the *X*d*Y* format (ie `2d20`) will yield a corresponding number of die rolls (up to 9).
  Repeated parameters (ie `2d6 2d8`) will result in multiple responses from Pardol. A `roll` command with missing or invalid parameters will give a snarky comment.
  The first parameter can optionally be a ruleset selector. Currently supported rulesets are `dnd` for D20-style dice, and `sta`, `trek` or `startrek` for Star Trek Adventures dice (STA dice are not yet functional).

## 0.2 - 0.2.2
[Commits since 0.1](https://gitlab.com/evogame/typecast/Pardol/-/compare/b0a2954b...ba4f8fac)
* Updates to CI config to fully employ Gitlab deployment features.
* Removal of colour IRC usernames as Twitch does not support this.
* Further tweaks to better work with Twitch.

## Pardol 0.1
[Commits since Dis4IRC 1.2.0](https://gitlab.com/evogame/typecast/Pardol/-/compare/v1.2.0...b0a2954b)
* Project forked to Pardol.

## 1.2.0 - `7766b34`
[Commits since 1.1.0](https://github.com/zachbr/Dis4IRC/compare/v1.1.0...v1.2.0)
* Discord Library Updates - **IMPORTANT**: You will be required to update to this version before November 7th 2020. That
  is when Discord will remove its old discordapp.com API endpoint.  
  
  As part of this update, Please note that Dis4IRC
  **REQUIRES** the `GUILD_MEMBERS` privileged intent in order to properly cache members at runtime.  
  **For instructions on adding the needed intent in the Discord Developer Console, please click [here](https://github.com/zachbr/Dis4IRC/blob/master/docs/Registering-A-Discord-Application.md#gateway-intents).**
* The webhook system now takes advantage of Discord API's _Allowed Mentions_ system, making it harder to abuse mentions.
* IRC users can now request all pinned messages from the bridged Discord channel using the `pinned` command.
* All bridge statistics are now persisted to disk, allowing you to restart the bridge without losing message statistics.
* Commands like `pinned` and `stats` can now be entirely disabled in the config file.
* The expiration time on pastes submitted by the paste service can now be configured.
* The IRC library was updated, fixing a reconnect issue with unstable connections.

## 1.1.0 - `5a3a45e`
[Commits since 1.0.2](https://github.com/zachbr/Dis4IRC/compare/v1.0.2...v1.1.0)
* The build date has been removed from the jar to support [reproducible builds](https://en.wikipedia.org/wiki/Reproducible_builds).
* The stats command will now show a percentage for each side of the bridge.
* The bridge will now exit in error if it cannot connect at startup.
* No-prefix messages can now optionally send an additional message with the triggering user's name.
* Better error messages for startup and connection failures.
* Fixes for mixed case IRC channel mappings.
* Fixes for startup IRC commands and additional logging.
* Fix for IRC nickname coloring issue.
* Add user quit and user kick relaying support.
* Updates to the underlying IRC and Discord libraries.

## 1.0.2 - 2019-01-02T23:30:28Z - `d4c6204`
[Commits since 1.0.1](https://github.com/zachbr/Dis4IRC/compare/v1.0.1...v1.0.2)
* Hotfix - Do not re-save config at startup as a workaround for [GH-19](https://github.com/zachbr/Dis4IRC/issues/19).

## 1.0.1 - 2018-12-31T05:16:47Z - `54f47af`
[Commits since 1.0.0](https://github.com/zachbr/Dis4IRC/compare/v1.0.0...v1.0.1)
* Better handling of whitespace-only messages for Discord.
* Statistics command now has a 60s rate limit on use.
* Respects guild-specific bot display name with webhooks.
* Markdown parser ignores messages shorter than 3 characters.

## 1.0.0 - 2018-11-22T01:43:07Z - `068f468`
* Initial Release.
