Pardol (A Dis4IRC Fork)
=======

A customized modern Discord <-> IRC Bridge based on the [Dis4IRC](https://github.com/zachbr/Dis4IRC) app.

Published under the [MIT License](https://gitlab.com/evogame/typecast/Pardol/-/blob/master/LICENSE.md)

Features
--------
* Allows realtime chat between IRC-based chat and a Discord server chanel.
* Markdown and Modern IRC Client Features
* Paste support for long messages
* Channel Join/Quit broadcasts
* Discord webhook support
* Non-prefixed messages for other IRC bots to handle
* IRC anti-ping zero width character in usernames
* [Integrated dice roller](https://gitlab.com/evogame/typecast/Pardol/-/wikis/home)
* Custom commands for various game action rolls


Getting Started
---------------
Please see the [Getting Started page](https://gitlab.com/evogame/typecast/Pardol/-/blob/master/docs/Getting-Started.md)

Example Config
--------------
```hocon
# Pardol Configuration File

# A list of bridges that Pardol should start up
# Each bridge can bridge multiple channels between a single IRC and Discord Server
bridges {
    # A bridge is a single bridged connection operating in its own space away from all the other bridges
    # Most people will only need this one default bridge
    default {
        # Relay joins, quits, parts, and kicks
        announce-joins-and-quits=false
        # Relay extra verbose information you don't really need like topics and mode changes.
        announce-extras=false
        # Mappings are the channel <-> channel bridging configurations
        channel-mappings {
            "712345611123456811"="#bridgedChannel"
        }
        # Your discord API key you registered your bot with
        discord-api-key="NTjhWZj1MTq0L10gMDU0MSQ1.Zpj02g.4QiWlNw9W5xd150qXsC3e-oc156"
        # Match a channel id to a webhook URL to enable webhooks for that channel
        discord-webhooks {
            "712345611123456811"="https://discordapp.com/api/webhooks/712345611123456811/blahblahurl"
        }
        commands {
            pinned {
                enabled="true"
            }
            stats {
                enabled="true"
            }
        }
        # Configuration for connecting to the IRC server
        irc {
            anti-ping=true
            nickname=TestBridge2
            # Messages that match this regular expression will be passed to IRC without a user prefix
            no-prefix-regex="^\\.[A-Za-z0-9]"
            # Sets the max context length to use for messages that are Discord replies. 0 to disable.
            discord-reply-context-limit=90
            # A list of __raw__ irc messages to send
            init-commands-list=[
                "PRIVMSG NICKSERV info",
                "PRIVMSG NICKSERV help"
            ]
            port="6697"
            realname=BridgeBot
            server="irc.esper.net"
            use-ssl=true
            username=BridgeBot
        }
        mutators {
            paste-service {
                max-message-length=450
                max-new-lines=4
                # Number of days before paste expires. Use 0 to never expire.
                paste-expiration-in-days=7
            }
        }
    }
}
debug-logging=true

```

The Name
--------
The name comes from a NPC minor death god character from the [TypecastRPG](https://www.typecastrpg.com/) D&D campaign __The Gods of Vaeron__, as performed by [Dan Wells](https://www.thedanwells.com/).

Built using
-----------
* [KittehIRCClientLib](https://github.com/KittehOrg/KittehIRCClientLib)
* [JDA (Java Discord API)](https://github.com/DV8FromTheWorld/JDA)
* [Configurate](https://github.com/SpongePowered/configurate)

